#include "Parsers/SpriteSheetParser.h"

#include <fstream>

std::shared_ptr<sprite::Sheet> SpriteSheetParser::parse(
    std::string jsonFileName) {
  nlohmann::json json;
  std::ifstream(jsonFileName) >> json;

  auto sheet = std::make_shared<sprite::Sheet>();
  sheet->texture = makeTexture(json);
  sheet->layers = makeLayers(json);
  return sheet;
}

std::shared_ptr<sf::Texture> SpriteSheetParser::makeTexture(
    const nlohmann::json& json) {
  std::string imageFile = json["meta"]["image"];
  auto texture = std::make_shared<sf::Texture>();
  texture->loadFromFile("./res/" + imageFile);
  texture->setRepeated(true);
  return texture;
}

ByTag<std::vector<sprite::Layer>> SpriteSheetParser::makeLayers(
    const nlohmann::json& json) {
  auto frameDefsByLayer = mapFrameDefsByLayerName(json);

  ByTag<std::vector<sprite::Layer>> layers;
  for (const auto& tagDef : json["meta"]["frameTags"]) {
    std::string tagName = tagDef["name"];
    size_t from = tagDef["from"];
    size_t to = tagDef["to"];

    for (const auto& layerDef : json["meta"]["layers"]) {
      std::string layerName = layerDef["name"];
      auto frameDefs = frameDefsByLayer[layerName];

      auto layer = makeLayer(layerName, frameDefs, from, to);
      layers[tagName].push_back(layer);
    }
  }
  return layers;
}

ByLayer<Definitions> SpriteSheetParser::mapFrameDefsByLayerName(
    const nlohmann::json& json) {
  ByLayer<Definitions> byLayerName;
  for (const nlohmann::json& item : json["frames"]) {
    std::string name = item["filename"];
    byLayerName[name].push_back(item);
  }
  return byLayerName;
}

sprite::Layer SpriteSheetParser::makeLayer(std::string name,
                                           Definitions frameDefinitions,
                                           size_t from, size_t to) {
  std::vector<sprite::Frame> frames;
  for (size_t i = from; i < to + 1; ++i) {
    frames.push_back(makeFrame(frameDefinitions[i]));
  }
  return sprite::Layer{name, frames};
}

sprite::Frame SpriteSheetParser::makeFrame(
    const nlohmann::json& frameDefinition) {
  const auto& duration = frameDefinition["duration"];
  const auto& frame = frameDefinition["frame"];
  sf::IntRect rect(frame["x"], frame["y"], frame["w"], frame["h"]);
  return sprite::Frame{rect, (float)duration / 1000.0f};
}