#ifndef SRC_PARSERS_SPRITESHEETPARSER_H
#define SRC_PARSERS_SPRITESHEETPARSER_H

#include <memory>
#include <nlohmann/json.hpp>
#include <string>
#include <vector>

#include "Components/Sprite.h"

template <class T>
using ByLayer = std::map<std::string, T>;

template <class T>
using ByTag = std::map<std::string, T>;

using Definitions = std::vector<nlohmann::json>;

class SpriteSheetParser {
 public:
  SpriteSheetParser() {}
  std::shared_ptr<sprite::Sheet> parse(std::string jsonFileName);

 private:
  std::shared_ptr<sf::Texture> makeTexture(const nlohmann::json& json);
  ByTag<std::vector<sprite::Layer>> makeLayers(const nlohmann::json& json);
  ByLayer<Definitions> mapFrameDefsByLayerName(const nlohmann::json& json);
  sprite::Layer makeLayer(std::string name, Definitions frameDefinitions,
                          size_t from, size_t to);
  sprite::Frame makeFrame(const nlohmann::json& frameDefinition);
};

#endif /* SRC_PARSERS_SPRITESHEETPARSER_H */
