#include "Systems/BallPossessionSystem.h"

#include <box2d/box2d.h>

#include "Builders/BodyBuilder.h"
#include "Builders/SignatureBuilder.hpp"
#include "Components/Components.h"
#include "Ecs/Ecs.hpp"

std::shared_ptr<BallPossessionSystem> BallPossessionSystem::create(Ecs* ecs) {
  SignatureBuilder signatureBuilder(ecs);
  signatureBuilder.setDependency<components::BallPossession>();
  Signature signature = signatureBuilder.get();

  auto system = ecs->registerSystem<BallPossessionSystem>();
  ecs->setSystemSignature<BallPossessionSystem>(signature);
  return system;
}

void BallPossessionSystem::update(Ecs* ecs, b2World* world, float seconds) {
  std::vector<Entity> broken;
  for (const auto& entity : entities) {
    auto& possession = ecs->getComponent<components::BallPossession>(entity);
    if (possession.joint == nullptr) {
      b2Vec2 anchorA{0.0f, 0.16f};
      b2Vec2 anchorB{0.0f, 0.0f};
      float length = 0.0f;
      float min = 0.0f;
      float max = 0.08f;
      float stiffness = 20.0f;

      BodyBuilder builder(world, possession.player);
      possession.joint = builder.addDistanceJoint(
          possession.ball, anchorA, anchorB, length, min, max, stiffness);
    } else {
      float force = possession.joint->GetReactionForce(1.0f / seconds).Length();
      if (force > 50.0f) {
        broken.push_back(entity);
      }
    }
  }
  for (const auto& entity : broken) {
    auto& possession = ecs->getComponent<components::BallPossession>(entity);
    world->DestroyJoint(possession.joint);
    ecs->removeComponent<components::BallPossession>(entity);
  }
}