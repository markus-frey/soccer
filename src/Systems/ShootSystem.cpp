#include "Systems/ShootSystem.h"

#include <box2d/b2_math.h>
#include <box2d/box2d.h>

#include "Builders/SignatureBuilder.hpp"
#include "Components/Components.h"
#include "Ecs/Ecs.hpp"

std::shared_ptr<ShootSystem> ShootSystem::create(Ecs* ecs) {
  SignatureBuilder signatureBuilder(ecs);
  signatureBuilder.setDependency<components::BallPossession>();
  signatureBuilder.setDependency<components::UserInput>();
  Signature signature = signatureBuilder.get();

  auto system = ecs->registerSystem<ShootSystem>();
  ecs->setSystemSignature<ShootSystem>(signature);
  return system;
}

void ShootSystem::update(Ecs* ecs, b2World* world, float seconds) {
  std::vector<Entity> broken;
  for (const auto& entity : entities) {
    auto& possession = ecs->getComponent<components::BallPossession>(entity);
    auto& ctrl = ecs->getComponent<components::UserInput>(entity);

    if (ctrl.shoot) {
      if (ctrl.shotCharge < 1.0f) {
        ctrl.shotCharge += seconds;
        ctrl.shotCharge = std::min(1.0f, ctrl.shotCharge);
      }
    } else if (ctrl.shotCharge > 0.0f) {
      b2Vec2 direction{ctrl.x, ctrl.y};
      if (direction.Length() < 0.01f) {  // Deadzone.
        direction = possession.player->GetWorldVector(b2Vec2{0.0f, 1.0f});
      }
      direction.Normalize();
      float totalStrength = 10.0f * ctrl.shotCharge;
      totalStrength = std::max(1.5f, totalStrength);
      b2Vec2 impulse = totalStrength * direction;
      b2Vec2 point = possession.ball->GetPosition();
      possession.ball->ApplyLinearImpulse(impulse, point, true);
      ctrl.shotCharge = 0.0f;
      broken.push_back(entity);
    }
  }

  for (const auto& entity : broken) {
    auto& possession = ecs->getComponent<components::BallPossession>(entity);
    world->DestroyJoint(possession.joint);
    ecs->removeComponent<components::BallPossession>(entity);
  }
}