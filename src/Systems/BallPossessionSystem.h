#ifndef SRC_SYSTEMS_BALLPOSSESSIONSYSTEM_H
#define SRC_SYSTEMS_BALLPOSSESSIONSYSTEM_H

#include <memory>

#include "Ecs/System.hpp"

class b2World;
class Ecs;

class BallPossessionSystem : public System {
 public:
  static std::shared_ptr<BallPossessionSystem> create(Ecs* ecs);
  void update(Ecs* ecs, b2World* world, float seconds);
};

#endif /* SRC_SYSTEMS_BALLPOSSESSIONSYSTEM_H */
