#ifndef SRC_SYSTEMS_SYSTEMS_H
#define SRC_SYSTEMS_SYSTEMS_H

#include "Systems/BallPossessionSystem.h"
#include "Systems/KeyboardInputSystem.h"
#include "Systems/MovementSystem.h"
#include "Systems/ShootSystem.h"
#include "Systems/SpriteMovementTagsSystem.h"
#include "Systems/SpriteRenderSystem.h"

#endif /* SRC_SYSTEMS_SYSTEMS_H */
