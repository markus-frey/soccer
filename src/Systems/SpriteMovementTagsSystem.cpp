#include "Systems/SpriteMovementTagsSystem.h"

#include <box2d/box2d.h>

#include "Builders/SignatureBuilder.hpp"
#include "Components/Components.h"
#include "Ecs/Ecs.hpp"

namespace {

void setTag(components::Sprite& sprite, std::string tag) {
  if (sprite.tag != tag) {
    sprite.tag = tag;
    for (auto& playback : sprite.playbacks) {
      playback.second.frame = 0;
      playback.second.duration = 0.0f;
    }
  }
}

}  // namespace

std::shared_ptr<SpriteMovementTagsSystem> SpriteMovementTagsSystem::create(
    Ecs* ecs) {
  SignatureBuilder signatureBuilder(ecs);
  signatureBuilder.setDependency<components::Body>();
  signatureBuilder.setDependency<components::Sprite>();
  signatureBuilder.setDependency<components::SpriteMovementTags>();
  Signature signature = signatureBuilder.get();

  auto system = ecs->registerSystem<SpriteMovementTagsSystem>();
  ecs->setSystemSignature<SpriteMovementTagsSystem>(signature);
  return system;
}

void SpriteMovementTagsSystem::update(Ecs* ecs) {
  for (const auto& entity : entities) {
    auto& body = ecs->getComponent<components::Body>(entity);
    auto& sprite = ecs->getComponent<components::Sprite>(entity);
    auto& animation = ecs->getComponent<components::SpriteMovementTags>(entity);

    float velocity = body.body->GetLinearVelocity().Length();
    if (velocity < animation.thresholdSlow) {
      setTag(sprite, animation.still);
    } else if (velocity < animation.thresholdFast) {
      setTag(sprite, animation.slow);
    } else {
      setTag(sprite, animation.fast);
    }
  }
}