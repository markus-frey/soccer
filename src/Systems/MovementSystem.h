#ifndef SRC_SYSTEMS_MOVEMENTSYSTEM_H
#define SRC_SYSTEMS_MOVEMENTSYSTEM_H

#include <memory>

#include "Ecs/System.hpp"

class Ecs;

class MovementSystem : public System {
 public:
  static std::shared_ptr<MovementSystem> create(Ecs* ecs);
  void update(Ecs* ecs);
};

#endif /* SRC_SYSTEMS_MOVEMENTSYSTEM_H */
