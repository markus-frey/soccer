#include "Systems/KeyboardInputSystem.h"

#include <SFML/Window/Keyboard.hpp>

#include "Builders/SignatureBuilder.hpp"
#include "Components/Components.h"
#include "Ecs/Ecs.hpp"

std::shared_ptr<KeyboardInputSystem> KeyboardInputSystem::create(Ecs* ecs) {
  SignatureBuilder signatureBuilder(ecs);
  signatureBuilder.setDependency<components::UserInput>();
  signatureBuilder.setDependency<components::KeyboardMapping>();
  Signature signature = signatureBuilder.get();

  auto system = ecs->registerSystem<KeyboardInputSystem>();
  ecs->setSystemSignature<KeyboardInputSystem>(signature);
  return system;
}

void KeyboardInputSystem::update(Ecs* ecs) {
  for (const auto& entity : entities) {
    auto& controlling = ecs->getComponent<components::UserInput>(entity);
    auto& input = ecs->getComponent<components::KeyboardMapping>(entity);

    controlling.x = sf::Keyboard::isKeyPressed(input.right) -
                    sf::Keyboard::isKeyPressed(input.left);
    controlling.y = sf::Keyboard::isKeyPressed(input.up) -
                    sf::Keyboard::isKeyPressed(input.down);
    controlling.sprint = sf::Keyboard::isKeyPressed(input.sprint);
    controlling.shoot = sf::Keyboard::isKeyPressed(input.shoot);
  }
}