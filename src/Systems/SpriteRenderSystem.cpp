#include "Systems/SpriteRenderSystem.h"

#include <box2d/b2_math.h>

#include <SFML/Graphics/RenderWindow.hpp>

#include "Builders/Builders.h"
#include "Components/Components.h"
#include "Ecs/Ecs.hpp"

namespace {

const std::vector<sprite::Layer>& getLayers(const sprite::Sheet& sheet,
                                            std::string tag) {
  const auto& layers = sheet.layers.find(tag);
  if (layers == sheet.layers.end()) {
    return sheet.layers.begin()->second;
  }
  return layers->second;
}

void drawFrame(sf::RenderWindow* window, const sprite::Sheet& sheet,
               const components::Body& body, const sprite::Frame& frame,
               const layer::Transform& transform,
               const layer::Playback& playback) {
  SfSpriteBuilder builder;
  builder.setTexture(*sheet.texture, frame, transform);
  builder.setPosition(body);
  builder.setScale(body, frame, transform);
  builder.setRotation(body, transform, playback);
  sf::Sprite sfSprite = builder.get();
  window->draw(sfSprite);
}

void updateDuration(float seconds, const components::Body& body,
                    sprite::Frame frame, layer::Playback& playback) {
  float velocity = body.body->GetLinearVelocity().Length();
  if (frame.duration > 0.0f) {
    playback.duration += seconds + playback.velocitySpeedup * velocity;
  } else {
    playback.duration += seconds * playback.velocitySpeedup * velocity;
  }
}

void updateFrame(float seconds, const components::Body& body,
                 const sprite::Layer& layer, const sprite::Frame& frame,
                 layer::Playback& playback) {
  if (playback.duration >= (frame.duration > 0 ? frame.duration : 1.0f)) {
    playback.frame = ++playback.frame % layer.frames.size();
    playback.duration = 0.0f;
  }
}

void updateMovementAngle(const components::Body& body,
                         layer::Playback& playback) {
  b2Vec2 movement = body.body->GetLinearVelocity();
  if (movement.Length() > 1e-3) {
    movement.Normalize();
    playback.movementAngle = b2Atan2(movement.y, movement.x) - 0.5 * b2_pi;
  }
}

}  // namespace

std::shared_ptr<SpriteRenderSystem> SpriteRenderSystem::create(Ecs* ecs) {
  SignatureBuilder signatureBuilder(ecs);
  signatureBuilder.setDependency<components::Body>();
  signatureBuilder.setDependency<components::Sprite>();
  Signature signature = signatureBuilder.get();

  auto system = ecs->registerSystem<SpriteRenderSystem>();
  ecs->setSystemSignature<SpriteRenderSystem>(signature);
  return system;
}

void SpriteRenderSystem::draw(Ecs* ecs, sf::RenderWindow* window) {
  for (const auto& entity : entities) {
    auto& body = ecs->getComponent<components::Body>(entity);
    auto& sprite = ecs->getComponent<components::Sprite>(entity);

    for (const auto& layer : getLayers(*sprite.sheet, sprite.tag)) {
      auto& playback = sprite.playbacks[layer.name];
      auto& transform = sprite.transforms[layer.name];
      const auto& frame = layer.frames[playback.frame];
      drawFrame(window, *sprite.sheet, body, frame, transform, playback);
    }
  }
}

void SpriteRenderSystem::update(Ecs* ecs, float seconds) {
  for (const auto& entity : entities) {
    auto& body = ecs->getComponent<components::Body>(entity);
    auto& sprite = ecs->getComponent<components::Sprite>(entity);

    for (const auto& layer : getLayers(*sprite.sheet, sprite.tag)) {
      auto& playback = sprite.playbacks[layer.name];
      auto& transform = sprite.transforms[layer.name];
      const auto& frame = layer.frames[playback.frame];
      updateDuration(seconds, body, frame, playback);
      updateFrame(seconds, body, layer, frame, playback);
      updateMovementAngle(body, playback);
    }
  }
}