#ifndef SRC_SYSTEMS_KEYBOARDINPUTSYSTEM_H
#define SRC_SYSTEMS_KEYBOARDINPUTSYSTEM_H

#include <memory>

#include "Ecs/System.hpp"

class Ecs;

class KeyboardInputSystem : public System {
 public:
  static std::shared_ptr<KeyboardInputSystem> create(Ecs* ecs);
  void update(Ecs* ecs);
};

#endif /* SRC_SYSTEMS_KEYBOARDINPUTSYSTEM_H */
