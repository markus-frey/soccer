#ifndef SRC_SYSTEMS_SPRITEMOVEMENTTAGSSYSTEM_H
#define SRC_SYSTEMS_SPRITEMOVEMENTTAGSSYSTEM_H

#include <memory>

#include "Ecs/System.hpp"

class Ecs;

class SpriteMovementTagsSystem : public System {
 public:
  static std::shared_ptr<SpriteMovementTagsSystem> create(Ecs* ecs);
  void update(Ecs* ecs);
};

#endif /* SRC_SYSTEMS_SPRITEMOVEMENTTAGSSYSTEM_H */
