#ifndef SRC_SYSTEMS_RENDERSYSTEM_H
#define SRC_SYSTEMS_RENDERSYSTEM_H

#include <memory>
#include <vector>

#include "Ecs/System.hpp"

class Ecs;

namespace sf {
class RenderWindow;
}  // namespace sf

class SpriteRenderSystem : public System {
 public:
  static std::shared_ptr<SpriteRenderSystem> create(Ecs* ecs);
  void draw(Ecs* ecs, sf::RenderWindow* window);
  void update(Ecs* ecs, float seconds);
};

#endif /* SRC_SYSTEMS_RENDERSYSTEM_H */
