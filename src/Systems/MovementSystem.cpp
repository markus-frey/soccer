#include "Systems/MovementSystem.h"

#include <box2d/b2_math.h>
#include <box2d/box2d.h>

#include "Builders/SignatureBuilder.hpp"
#include "Components/Components.h"
#include "Ecs/Ecs.hpp"

std::shared_ptr<MovementSystem> MovementSystem::create(Ecs* ecs) {
  SignatureBuilder signatureBuilder(ecs);
  signatureBuilder.setDependency<components::Body>();
  signatureBuilder.setDependency<components::UserInput>();
  Signature signature = signatureBuilder.get();

  auto system = ecs->registerSystem<MovementSystem>();
  ecs->setSystemSignature<MovementSystem>(signature);
  return system;
}

void MovementSystem::update(Ecs* ecs) {
  for (const auto& entity : entities) {
    auto& body = ecs->getComponent<components::Body>(entity);
    auto& ctrl = ecs->getComponent<components::UserInput>(entity);

    b2Vec2 direction{ctrl.x, ctrl.y};
    direction.Normalize();
    if (ctrl.shoot) {
      direction = body.body->GetWorldVector(b2Vec2{0.0f, 1.0f});
    }

    b2Vec2 vel = body.body->GetLinearVelocity();
    float speed = vel.Normalize();
    if (speed > 1e-3) {
      float angle = b2Atan2(vel.y, vel.x) - 0.5 * b2_pi;
      body.body->SetTransform(body.body->GetPosition(), angle);
    }

    if (ctrl.sprint && ctrl.shoot == false) {
      body.body->ApplyForceToCenter(1000.0f * direction, true);
    } else {
      body.body->ApplyForceToCenter(400.0f * direction, true);
    }
    body.body->ApplyForceToCenter(50.0f * speed * speed * -vel, true);
  }
}