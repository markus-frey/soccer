#ifndef SRC_SYSTEMS_SHOOTSYSTEM_H
#define SRC_SYSTEMS_SHOOTSYSTEM_H

#include <memory>

#include "Ecs/System.hpp"

class Ecs;
class b2World;

class ShootSystem : public System {
 public:
  static std::shared_ptr<ShootSystem> create(Ecs* ecs);
  void update(Ecs* ecs, b2World* world, float seconds);
};

#endif /* SRC_SYSTEMS_SHOOTSYSTEM_H */
