#include <SFML/Graphics.hpp>
#include <stack>

#include "Game/Stats.hpp"
#include "Game/World.h"

class Window {
  std::stack<sf::View> viewStack;

 public:
  sf::RenderWindow renderWindow;

  Window(float width, float height)
      : renderWindow(sf::VideoMode(width, height), "Soccer") {
    renderWindow.setFramerateLimit(60);
    push(renderWindow.getDefaultView());
  }

  void push(sf::FloatRect rect) {
    sf::Vector2f windowSize = sf::Vector2f(renderWindow.getSize());
    float windowAspect = windowSize.y / windowSize.x;
    float rectAspect = rect.height / rect.width;
    if (rectAspect > windowAspect) {
      rect.width /= windowAspect;
    }
    push(sf::View(sf::Vector2f(rect.left, rect.top),
                  sf::Vector2f(rect.width, rect.height)));
  }

  void push(sf::View view) {
    viewStack.push(view);
    renderWindow.setView(view);
  }

  void pop() {
    viewStack.pop();
    renderWindow.setView(viewStack.top());
  }
};

int main() {
  Window window(800, 600);
  Stats stats;
  World world;

  sf::FloatRect fieldRect(0.0f, 0.0f, 6.0f, 6.0f);

  sf::Clock clock;
  float seconds = 0.0f;
  while (window.renderWindow.isOpen()) {
    world.update(seconds);
    stats.update(seconds);

    window.renderWindow.clear(sf::Color::Black);
    window.push(fieldRect);
    world.draw(&window.renderWindow);
    window.pop();
    stats.draw(&window.renderWindow);
    window.renderWindow.display();

    sf::Event event;
    while (window.renderWindow.pollEvent(event)) {
      bool closeByKeyPress = event.type == sf::Event::KeyPressed &&
                             (event.key.code == sf::Keyboard::Escape ||
                              event.key.code == sf::Keyboard::Enter);
      if (closeByKeyPress || event.type == sf::Event::Closed) {
        window.renderWindow.close();
      }
    }

    seconds = clock.restart().asSeconds();
  }
  return 0;
}