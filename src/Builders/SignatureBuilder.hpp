#ifndef SRC_BUILDERS_SIGNATUREBUILDER_HPP
#define SRC_BUILDERS_SIGNATUREBUILDER_HPP

#include <memory>

#include "Ecs/Ecs.hpp"

class SignatureBuilder {
  Ecs* ecs;
  Signature signature;

 public:
  SignatureBuilder(Ecs* ecs) : ecs(ecs) {}

  void reset() { signature.reset(); }

  template <typename T>
  void setDependency() {
    signature.set(ecs->getComponentType<T>());
  }

  Signature get() { return signature; }
};

#endif /* SRC_BUILDERS_SIGNATUREBUILDER_HPP */
