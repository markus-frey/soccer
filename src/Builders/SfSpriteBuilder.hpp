#ifndef SRC_BUILDERS_SFSPRITEBUILDER_HPP
#define SRC_BUILDERS_SFSPRITEBUILDER_HPP

#include "Components/Components.h"

class SfSpriteBuilder {
 public:
  void setPosition(const components::Body& body) {
    const b2Vec2& position = body.body->GetPosition();
    sprite.setPosition(position.x, -position.y);
  }

  void setTexture(const sf::Texture& texture, const sprite::Frame& frame,
                  const layer::Transform& transform) {
    float width = frame.rect.width * transform.repeatX;
    float height = frame.rect.height * transform.repeatY;
    sf::IntRect rect(frame.rect.left, frame.rect.top, width, height);
    sprite.setTexture(texture);
    sprite.setTextureRect(rect);
    sprite.setOrigin(rect.width * 0.5f, rect.height * 0.5f);
  }

  void setScale(const components::Body& body, const sprite::Frame& frame,
                const layer::Transform& transform) {
    float width = frame.rect.width * transform.repeatX;
    float height = frame.rect.height * transform.repeatY;
    float scaleX = body.width / width;
    float scaleY = body.height / height;
    sprite.setScale(scaleX, scaleY);
  }

  void setRotation(const components::Body& body,
                   const layer::Transform& transform,
                   const layer::Playback& playback) {
    float angle = transform.angle;
    if (transform.reference == transform.BODY) {
      angle += body.body->GetAngle();
    } else if (transform.reference == transform.MOVEMENT) {
      angle += playback.movementAngle;
    }
    sprite.setRotation(-180.0f * angle / b2_pi);
  }

  sf::Sprite get() { return sprite; }

 private:
  sf::Sprite sprite;
};

#endif /* SRC_BUILDERS_SFSPRITEBUILDER_HPP */
