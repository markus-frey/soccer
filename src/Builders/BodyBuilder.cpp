#include "Builders/BodyBuilder.h"

#include <iostream>

void BodyBuilder::create(b2BodyType type, float x, float y) {
  b2BodyDef def;
  def.type = type;
  def.position.Set(x, y);
  def.linearDamping = 0.0f;
  def.angularDamping = 0.0f;
  body = world->CreateBody(&def);
}

b2Body* BodyBuilder::get() { return body; }

b2Fixture* BodyBuilder::addBox(float hx, float hy, float density,
                               float friction, float restitution) {
  b2PolygonShape shape;
  shape.SetAsBox(hx, hy);
  return createFixture(&shape, density, friction, restitution);
}

b2Fixture* BodyBuilder::addCircle(float r, float density, float friction,
                                  float restitution) {
  b2CircleShape shape;
  shape.m_radius = r;
  return createFixture(&shape, density, friction, restitution);
}

b2Fixture* BodyBuilder::createFixture(const b2Shape* shape, float density,
                                      float friction, float restitution) {
  b2FixtureDef def;
  def.shape = shape;
  def.density = density;
  def.friction = friction;
  def.restitution = restitution;
  return body->CreateFixture(&def);
}

b2Joint* BodyBuilder::addFrictionJoint(b2Body* bodyB, float maxForce,
                                       float maxTorque) {
  b2FrictionJointDef def;
  def.Initialize(body, bodyB, body->GetWorldCenter());
  def.maxForce = maxForce;
  def.maxTorque = maxTorque;
  return world->CreateJoint(&def);
}

b2Joint* BodyBuilder::addDistanceJoint(b2Body* bodyB, b2Vec2 anchorA,
                                       b2Vec2 anchorB, float length, float min,
                                       float max, float stiffness) {
  b2DistanceJointDef def;
  def.Initialize(body, bodyB, anchorA, anchorB);
  def.localAnchorA = anchorA;
  def.localAnchorB = anchorB;
  def.length = length;
  def.minLength = min;
  def.maxLength = max;
  def.stiffness = stiffness;
  return world->CreateJoint(&def);
}

void BodyBuilder::setUserData(const b2BodyUserData& userData) {
  body->GetUserData() = userData;
}

void BodyBuilder::setFixedRotation(bool flag) { body->SetFixedRotation(flag); }