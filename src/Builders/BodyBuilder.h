#ifndef SRC_BUILDERS_BODYBUILDER_H
#define SRC_BUILDERS_BODYBUILDER_H

#include <box2d/box2d.h>

class BodyBuilder {
 public:
  BodyBuilder(b2World* world, b2Body* body = nullptr)
      : world(world), body(body) {}

  void create(b2BodyType type, float x, float y);
  b2Body* get();

  b2Fixture* addBox(float hx, float hy, float density, float friction,
                    float restitution);
  b2Fixture* addCircle(float r, float density, float friction,
                       float restitution);
  b2Joint* addFrictionJoint(b2Body* bodyB, float maxForce, float maxTorque);
  b2Joint* addDistanceJoint(b2Body* bodyB, b2Vec2 anchorA, b2Vec2 anchorB,
                            float length, float min, float max,
                            float stiffness);
  void setUserData(const b2BodyUserData& userData);
  void setFixedRotation(bool flag);

 private:
  b2Fixture* createFixture(const b2Shape* shape, float density, float friction,
                           float restitution);
  b2World* world;
  b2Body* body;
};

#endif /* SRC_BUILDERS_BODYBUILDER_H */
