#ifndef SRC_GAME_WORLD_H
#define SRC_GAME_WORLD_H

#include <box2d/b2_world.h>

#include <memory>

#include "Builders/Builders.h"
#include "Ecs/Ecs.hpp"
#include "Game/ContactListener.hpp"

namespace sf {
class RenderWindow;
}

class SpriteRenderSystem;
class BallPossessionSystem;
class KeyboardInputSystem;
class MovementSystem;
class ShootSystem;
class SpriteMovementTagsSystem;

class World {
 public:
  World();

  void update(float seconds);
  void draw(sf::RenderWindow* window);

 private:
  b2World world;
  ContactListener contactListener;
  BodyBuilder bodyBuilder;

  Ecs ecs;
  std::shared_ptr<SpriteRenderSystem> spriteRenderSystem;
  std::shared_ptr<BallPossessionSystem> ballPossessionSystem;
  std::shared_ptr<KeyboardInputSystem> keyboardInputSystem;
  std::shared_ptr<MovementSystem> movementSystem;
  std::shared_ptr<ShootSystem> shootSystem;
  std::shared_ptr<SpriteMovementTagsSystem> spriteMovementTagsSystem;
};

#endif /* SRC_GAME_WORLD_H */
