#include "Game/World.h"

#include "Components/Components.h"
#include "Parsers/SpriteSheetParser.h"
#include "Systems/Systems.h"

void createBoundaries(Ecs* ecs, BodyBuilder* b, float hx, float hy) {
  float halfThickness = 0.05f;
  b->create(b2_staticBody, 0.0f, hy + halfThickness);  // Top.
  b->setUserData(b2BodyUserData(b2BodyUserData::BOUNDARY));
  b->addBox(hx, halfThickness, 0.0f, 0.0f, 0.0f);

  b->create(b2_staticBody, 0.0f, -(hy + halfThickness));  // Bottom.
  b->setUserData(b2BodyUserData(b2BodyUserData::BOUNDARY));
  b->addBox(hx, halfThickness, 0.0f, 0.0f, 0.0f);

  b->create(b2_staticBody, -(hx + halfThickness), 0.0f);  // Left.
  b->setUserData(b2BodyUserData(b2BodyUserData::BOUNDARY));
  b->addBox(halfThickness, hy, 0.0f, 0.0f, 0.0f);

  b->create(b2_staticBody, hx + halfThickness, 0.0f);  // Right.
  b->setUserData(b2BodyUserData(b2BodyUserData::BOUNDARY));
  b->addBox(halfThickness, hy, 0.0f, 0.0f, 0.0f);

  SpriteSheetParser jsonParser;
  b->create(b2_kinematicBody, 0.0f, 0.0f);
  Entity entity = ecs->createEntity();
  components::Body grassBody{b->get(), 2.0f * hx, 2.0f * hy};
  ecs->addComponent(entity, grassBody);
  components::Sprite grassSprite;
  grassSprite.sheet = jsonParser.parse("./res/sheets/grass.json");
  grassSprite.transforms["ground"].repeatX = 15;
  grassSprite.transforms["ground"].repeatY = 15;
  ecs->addComponent(entity, grassSprite);
}

void populate(Ecs* ecs, BodyBuilder* b) {
  SpriteSheetParser jsonParser;

  // Ground body for friction joints.
  b->create(b2_kinematicBody, 0.0f, 0.0f);
  b2Body* ground = b->get();

  // Ball.
  Entity entity = ecs->createEntity();
  b->create(b2_dynamicBody, 0.0f, 0.0f);
  b->setUserData(b2BodyUserData(b2BodyUserData::BALL));
  b->addCircle(0.07f, 30.0f, 0.0f, 0.7f);
  b->addFrictionJoint(ground, 0.7f, 0.0f);
  components::Body ballBody{b->get(), 0.14f, 0.14f};
  ecs->addComponent(entity, ballBody);

  components::Sprite ballSprite;
  ballSprite.sheet = jsonParser.parse("./res/sheets/ball.json");
  ballSprite.playbacks["pattern"].velocitySpeedup = 20.0f;
  ballSprite.transforms["pattern"].reference = layer::Transform::MOVEMENT;
  ballSprite.transforms["ball"].reference = layer::Transform::WORLD;
  ecs->addComponent(entity, ballSprite);

  ballBody.body->ApplyLinearImpulseToCenter({3, 5}, true);

  // Player.
  entity = ecs->createEntity();
  b->create(b2_dynamicBody, -2.0f, 0.0f);
  b->addCircle(0.15f, 600.0f, 0.8f, 0.0f);
  b->addFrictionJoint(ground, 100.0f, 0.0f);
  b->setFixedRotation(true);
  b->setUserData(b2BodyUserData(b2BodyUserData::PLAYER, entity));
  ecs->addComponent(entity, components::Body{b->get(), 0.3f, 0.3f});

  components::Sprite playerSprite;
  playerSprite.sheet = jsonParser.parse("./res/sheets/player.json");
  ecs->addComponent(entity, playerSprite);
  ecs->addComponent(entity, components::SpriteMovementTags{"idle", "walk",
                                                           "run", 0.1f, 3.0f});
  ecs->addComponent(entity,
                    components::UserInput{0.0f, 0.0f, false, false, 0.0f});
  ecs->addComponent(entity, components::KeyboardMapping{
                                sf::Keyboard::Up, sf::Keyboard::Right,
                                sf::Keyboard::Down, sf::Keyboard::Left,
                                sf::Keyboard::LShift, sf::Keyboard::Space});
}

World::World()
    : world(b2Vec2(0.0f, 0.0f)), contactListener(&ecs), bodyBuilder(&world) {
  world.SetContactListener(&contactListener);

  components::registerComponents(&ecs);
  spriteRenderSystem = SpriteRenderSystem::create(&ecs);
  ballPossessionSystem = BallPossessionSystem::create(&ecs);
  keyboardInputSystem = KeyboardInputSystem::create(&ecs);
  movementSystem = MovementSystem::create(&ecs);
  shootSystem = ShootSystem::create(&ecs);
  spriteMovementTagsSystem = SpriteMovementTagsSystem::create(&ecs);

  sf::FloatRect fieldRect(0.0f, 0.0f, 6.0f, 6.0f);
  createBoundaries(&ecs, &bodyBuilder, 0.5f * fieldRect.width,
                   0.5f * fieldRect.height);
  populate(&ecs, &bodyBuilder);
}

void World::update(float seconds) {
  world.Step(seconds, 6, 2);

  keyboardInputSystem->update(&ecs);
  movementSystem->update(&ecs);
  spriteMovementTagsSystem->update(&ecs);
  ballPossessionSystem->update(&ecs, &world, seconds);
  shootSystem->update(&ecs, &world, seconds);
  spriteRenderSystem->update(&ecs, seconds);
}

void World::draw(sf::RenderWindow* window) {
  spriteRenderSystem->draw(&ecs, window);
}