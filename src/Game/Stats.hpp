#ifndef SRC_GAME_STATS_HPP
#define SRC_GAME_STATS_HPP

#include <SFML/Graphics.hpp>

class Stats {
 public:
  Stats() : count(0), duration(0.0f) {
    font.loadFromFile("./res/ProggyTiny.ttf");
    text.setFont(font);
    text.setCharacterSize(16);
    text.setFillColor(sf::Color::White);
    text.setStyle(sf::Text::Regular);
    text.setPosition({10, 10});
  }

  void draw(sf::RenderWindow* window) { window->draw(text); }

  void update(float seconds) {
    if (duration < 1.0f) {
      count++;
      duration += seconds;
    } else {
      float fps = count / duration;
      count = 0;
      duration = 0.0f;
      char fpsText[16];
      snprintf(fpsText, 16, "fps: %.1f", fps);
      text.setString(fpsText);
    }
  }

 private:
  sf::Font font;
  sf::Text text;
  unsigned int count;
  float duration;
};

#endif /* SRC_GAME_STATS_HPP */
