#ifndef SRC_GAME_CONTACTLISTENER_HPP
#define SRC_GAME_CONTACTLISTENER_HPP

#include <box2d/box2d.h>

#include "Components/Components.h"

// TODO: Use decorator pattern.
class ContactListener : public b2ContactListener {
  Ecs* ecs;

 public:
  ContactListener(Ecs* ecs) : ecs(ecs) {}
  virtual void BeginContact(b2Contact* contact) {
    b2Body* a = contact->GetFixtureA()->GetBody();
    b2Body* b = contact->GetFixtureB()->GetBody();
    b2BodyUserData::Type aT = a->GetUserData().type;
    b2BodyUserData::Type bT = b->GetUserData().type;
    if (aT == b2BodyUserData::PLAYER && bT == b2BodyUserData::BALL) {
      OnContact(a, b);
    } else if (bT == b2BodyUserData::PLAYER && aT == b2BodyUserData::BALL) {
      OnContact(b, a);
    }
  }

  void OnContact(b2Body* player, b2Body* ball) {
    const Entity& e = player->GetUserData().entity;
    if (ecs->hasComponent<components::BallPossession>(e) == false ||
        ecs->getComponent<components::BallPossession>(e).player != player) {
      ecs->addComponent(e, components::BallPossession{player, ball, nullptr});
    }
  }
};

#endif /* SRC_GAME_CONTACTLISTENER_HPP */
