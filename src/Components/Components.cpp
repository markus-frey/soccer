#include "Components.h"

#include "Ecs/Ecs.hpp"

namespace components {

void registerComponents(Ecs* ecs) {
  ecs->registerComponent<Body>();
  ecs->registerComponent<Sprite>();
  ecs->registerComponent<SpriteMovementTags>();
  ecs->registerComponent<UserInput>();
  ecs->registerComponent<KeyboardMapping>();
  ecs->registerComponent<BallPossession>();
}

}  // namespace components