#ifndef SRC_COMPONENTS_LAYER_H
#define SRC_COMPONENTS_LAYER_H

namespace layer {

struct Playback {
  Playback() {
    frame = 0;
    duration = 0.0f;
    velocitySpeedup = 0.0f;
  }
  size_t frame;
  float duration;
  float movementAngle;
  float velocitySpeedup;
};

struct Transform {
  Transform() {
    angle = 0.0f;
    reference = BODY;
    repeatX = 1;
    repeatY = 1;
  }
  typedef enum { WORLD, BODY, MOVEMENT } Reference;
  float angle;
  Reference reference;
  float repeatX;
  float repeatY;
};

}  // namespace layer

#endif /* SRC_COMPONENTS_LAYER_H */
