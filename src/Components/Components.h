#ifndef SRC_COMPONENTS_COMPONENTS_H
#define SRC_COMPONENTS_COMPONENTS_H

#include <SFML/Graphics.hpp>
#include <memory>
#include <vector>

#include "Components/Layer.h"
#include "Components/Sprite.h"

class Ecs;
class b2Body;
class b2Joint;

namespace components {

void registerComponents(Ecs* ecs);

struct Body {
  b2Body* body;
  float width;
  float height;
};

struct Sprite {
  std::shared_ptr<sprite::Sheet> sheet;
  std::map<std::string, layer::Playback> playbacks;
  std::map<std::string, layer::Transform> transforms;
  std::string tag;
};

struct SpriteMovementTags {
  std::string still;
  std::string slow;
  std::string fast;
  float thresholdSlow;
  float thresholdFast;
};

struct BallPossession {
  b2Body* player;
  b2Body* ball;
  b2Joint* joint;
};

struct UserInput {
  float x;
  float y;
  bool sprint;
  bool shoot;
  float shotCharge;
};

struct KeyboardMapping {
  sf::Keyboard::Key up;
  sf::Keyboard::Key right;
  sf::Keyboard::Key down;
  sf::Keyboard::Key left;
  sf::Keyboard::Key sprint;
  sf::Keyboard::Key shoot;
};

}  // namespace components

#endif /* SRC_COMPONENTS_COMPONENTS_H */
