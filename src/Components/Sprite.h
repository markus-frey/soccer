#ifndef SRC_COMPONENTS_SPRITESHEET_H
#define SRC_COMPONENTS_SPRITESHEET_H

#include <SFML/Graphics.hpp>
#include <map>
#include <memory>
#include <string>
#include <vector>

namespace sprite {

template <class T>
using ByTag = std::map<std::string, T>;

struct Frame {
  sf::IntRect rect;
  float duration;
};

struct Layer {
  std::string name;
  std::vector<Frame> frames;
};

struct Sheet {
  std::shared_ptr<sf::Texture> texture;
  ByTag<std::vector<Layer>> layers;  // Vector to preserve order.
};

}  // namespace sprite

#endif /* SRC_COMPONENTS_SPRITESHEET_H */
