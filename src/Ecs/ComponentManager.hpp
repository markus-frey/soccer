#ifndef SRC_ECS_COMPONENTMANAGER_HPP
#define SRC_ECS_COMPONENTMANAGER_HPP

#include <any>
#include <memory>
#include <unordered_map>

#include "Ecs/ComponentArray.hpp"
#include "Ecs/Types.hpp"

class ComponentManager {
 public:
  template <typename T>
  void registerComponent() {
    const char* typeName = typeid(T).name();

    assert(componentTypes.find(typeName) == componentTypes.end() &&
           "Registering component type more than once.");

    componentTypes.insert({typeName, nextComponentType});
    componentArrays.insert({typeName, std::make_shared<ComponentArray<T>>()});

    ++nextComponentType;
  }

  template <typename T>
  ComponentType getComponentType() {
    const char* typeName = typeid(T).name();

    assert(componentTypes.find(typeName) != componentTypes.end() &&
           "Component not registered before use.");

    return componentTypes[typeName];
  }

  template <typename T>
  void addComponent(Entity entity, T component) {
    getComponentArray<T>()->insert(entity, component);
  }

  template <typename T>
  void removeComponent(Entity entity) {
    getComponentArray<T>()->remove(entity);
  }

  template <typename T>
  T& getComponent(Entity entity) {
    return getComponentArray<T>()->get(entity);
  }

  template <typename T>
  bool hasComponent(Entity entity) {
    return getComponentArray<T>()->contains(entity);
  }

  void onEntityDestroyed(Entity entity) {
    for (auto const& pair : componentArrays) {
      auto const& component = pair.second;
      component->onEntityDestroyed(entity);
    }
  }

 private:
  std::unordered_map<const char*, ComponentType> componentTypes{};
  std::unordered_map<const char*, std::shared_ptr<IComponentArray>>
      componentArrays{};
  ComponentType nextComponentType{};

  template <typename T>
  std::shared_ptr<ComponentArray<T>> getComponentArray() {
    const char* typeName = typeid(T).name();

    assert(componentTypes.find(typeName) != componentTypes.end() &&
           "Component not registered before use.");

    return std::static_pointer_cast<ComponentArray<T>>(
        componentArrays[typeName]);
  }
};

#endif /* SRC_ECS_COMPONENTMANAGER_HPP */
