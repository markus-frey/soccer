#ifndef SRC_ECS_TYPES_HPP
#define SRC_ECS_TYPES_HPP

#include <bitset>
#include <cstdint>

using Entity = std::uint32_t;
const Entity MAX_ENTITIES = 5000;
using ComponentType = std::uint8_t;
const ComponentType MAX_COMPONENTS = 32;
using Signature = std::bitset<MAX_COMPONENTS>;

#endif /* SRC_ECS_TYPES_HPP */
