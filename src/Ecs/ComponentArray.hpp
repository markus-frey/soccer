#ifndef SRC_ECS_COMPONENTARRAY_HPP
#define SRC_ECS_COMPONENTARRAY_HPP

#include <array>
#include <cassert>
#include <unordered_map>

#include "Ecs/Types.hpp"

class IComponentArray {
 public:
  virtual ~IComponentArray() = default;
  virtual void onEntityDestroyed(Entity entity) = 0;
};

template <typename T>
class ComponentArray : public IComponentArray {
 public:
  void insert(Entity entity, T component) {
    assert(entityToIndexMap.find(entity) == entityToIndexMap.end() &&
           "Component added to same entity more than once.");

    // Put new entry at end.
    size_t newIndex = size;
    entityToIndexMap[entity] = newIndex;
    indexToEntityMap[newIndex] = entity;
    componentArray[newIndex] = component;
    ++size;
  }

  void remove(Entity entity) {
    assert(entityToIndexMap.find(entity) != entityToIndexMap.end() &&
           "Removing non-existent component.");

    // Copy element at end into deleted element's place to maintain density.
    size_t indexOfRemovedEntity = entityToIndexMap[entity];
    size_t indexOfLastElement = size - 1;
    componentArray[indexOfRemovedEntity] = componentArray[indexOfLastElement];

    // Update map to point to moved spot.
    Entity entityOfLastElement = indexToEntityMap[indexOfLastElement];
    entityToIndexMap[entityOfLastElement] = indexOfRemovedEntity;
    indexToEntityMap[indexOfRemovedEntity] = entityOfLastElement;

    entityToIndexMap.erase(entity);
    indexToEntityMap.erase(indexOfLastElement);

    --size;
  }

  T& get(Entity entity) {
    assert(entityToIndexMap.find(entity) != entityToIndexMap.end() &&
           "Retrieving non-existent component.");

    return componentArray[entityToIndexMap[entity]];
  }

  bool contains(Entity entity) {
    return entityToIndexMap.find(entity) != entityToIndexMap.end();
  }

  void onEntityDestroyed(Entity entity) override {
    if (entityToIndexMap.find(entity) != entityToIndexMap.end()) {
      remove(entity);
    }
  }

 private:
  std::array<T, MAX_ENTITIES> componentArray{};
  std::unordered_map<Entity, size_t> entityToIndexMap{};
  std::unordered_map<size_t, Entity> indexToEntityMap{};
  size_t size{};
};

#endif /* SRC_ECS_COMPONENTARRAY_HPP */
