#ifndef SRC_ECS_SYSTEM_HPP
#define SRC_ECS_SYSTEM_HPP

#include <set>

#include "Ecs/Types.hpp"

class System {
 public:
  std::set<Entity> entities;
};

#endif /* SRC_ECS_SYSTEM_HPP */
