#ifndef SRC_ECS_ENTITYMANAGER_HPP
#define SRC_ECS_ENTITYMANAGER_HPP

#include <array>
#include <cassert>
#include <queue>

#include "Ecs/Types.hpp"

class EntityManager {
 public:
  EntityManager() {
    for (Entity entity = 0; entity < MAX_ENTITIES; ++entity) {
      availableEntities.push(entity);
    }
  }

  Entity createEntity() {
    assert(livingEntityCount < MAX_ENTITIES &&
           "Too many entities in existence.");

    Entity id = availableEntities.front();
    availableEntities.pop();
    ++livingEntityCount;

    return id;
  }

  void destroyEntity(Entity entity) {
    assert(entity < MAX_ENTITIES && "Entity out of range.");

    signatures[entity].reset();
    availableEntities.push(entity);
    --livingEntityCount;
  }

  void setSignature(Entity entity, Signature signature) {
    assert(entity < MAX_ENTITIES && "Entity out of range.");

    signatures[entity] = signature;
  }

  Signature getSignature(Entity entity) {
    assert(entity < MAX_ENTITIES && "Entity out of range.");

    return signatures[entity];
  }

 private:
  std::queue<Entity> availableEntities{};
  std::array<Signature, MAX_ENTITIES> signatures{};
  uint32_t livingEntityCount{};
};

#endif /* SRC_ECS_ENTITYMANAGER_HPP */
