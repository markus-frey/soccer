# :soccer: Soccer

## :wrench: Build

```bash
pip install conan
```

```bash
mkdir build; cd build;
conan install ..
conan build ..
```

### :pencil2: VSCode
When using `VSCode` with the `C/C++` and `CMake Tools` extension, set the following settings:
```json
{
"cmake.configureOnEdit": false,
"cmake.configureOnOpen": false,
"cmake.skipConfigureIfCachePresent": true
}
```
Afterwards, build from within `VSCode` in order for the `C/C++` extension to properly find include directories of all `conan` dependencies.

## :construction_worker: Todo
- Introduce namespaces to reduce class name lengths
- When picking up the ball use a collision zone that is larger than the player's body
- Introduce a player stats component holding speed, shot strength, etc.
- Introduce a ball stats component holding flags set by player's special powers or shots?
- Make box2d bodies hold entities. On collision, no specific body types need to be checked but instead check for existing entity components (player stats and ball stats)
- Make a player's head turn towards the ball

## :heart: Acknowledgments
- [SFML](https://www.sfml-dev.org)
- [Box2D](https://www.box2d.org)
- [JSON for Modern C++](https://github.com/nlohmann/json)
- [Austin Morlan - A Simple Entity Component System (ECS) [C++]](https://austinmorlan.com/posts/entity_component_system/)
- [Robert Nystrom - Game Programming Patterns](https://gameprogrammingpatterns.com/)
