from conan import ConanFile
from conan.tools.cmake import CMake


class SoccerConan(ConanFile):
    name = "soccer"
    version = "0.1.0"
    license = "<Put the package license here>"
    author = "Markus Frey maggifrey@gmx.de"
    url = ""
    description = ""
    topics = ()
    settings = "os", "compiler", "build_type", "arch"
    options = {"shared": [True, False], "fPIC": [True, False]}
    default_options = {"shared": False, "fPIC": True}
    generators = "cmake"
    requires = "sfml/2.5.1", "box2d/2.4.1", "nlohmann_json/3.9.1"

    def configure(self):
        if self.settings.compiler == "msvc":
            del self.options.fPIC

    def source(self):
        pass

    def build(self):
        cmake = CMake(self)
        cmake.configure()
        cmake.build()

    def package(self):
        pass

    def package_info(self):
        pass
